#!/bin/bash

TARGET_SRC=${PWD}

if [ -z "$ANDROID_HOME" ]
then
    ANDROID_SDK=${HOME}/Android/Sdk
else
    ANDROID_SDK=${ANDROID_HOME}
fi

CMAKE_EXE=${ANDROID_SDK}/cmake/3.10.2.4988404/bin/cmake

if [ -z "$ANDROID_NDK_HOME" ]
then
    ANDROID_NDK=${ANDROID_SDK}/ndk-bundle
else
    ANDROID_NDK=${ANDROID_NDK_HOME}
fi

if [ -z "$1" ]
then
    TARGET_ABI=arm64-v8a
else
    TARGET_ABI=$1
fi

${CMAKE_EXE} -H${TARGET_SRC} \
    -B${TARGET_SRC}/build/android/${TARGET_ABI} \
    -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=${TARGET_ABI} \
    -DANDROID_PLATFORM=android-21 \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_SYSTEM_NAME=Android \
    -DCMAKE_MAKE_PROGRAM=${ANDROID_NDK}/prebuilt/linux-x86_64/bin/make \
    -DANDROID_STL=c++_static 

${CMAKE_EXE} --build ${TARGET_SRC}/build/android/${TARGET_ABI}
