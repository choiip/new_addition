namespace tcl
{
namespace addition
{
class Engine
{
public:
    Engine();
    ~Engine();

    void process(int a, int b);

    int result() const;

private:
    int _result;
};
} // namespace addition
} // namespace tcl
