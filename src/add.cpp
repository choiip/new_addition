#include "add.hpp"

namespace tcl
{
namespace addition
{
Engine::Engine() : _result(0) {}
Engine::~Engine() = default;

void Engine::process(int a, int b)
{
    _result = a + b;
}

int Engine::result() const
{
    return _result;
}

} // namespace addition
} // namespace tcl
