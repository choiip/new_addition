#include <gtest/gtest.h>
#include <add.hpp>

TEST(addsumTest, OneAddZeroInput)
{
    tcl::addition::Engine engine;
    engine.process(1, 0);
    EXPECT_EQ(engine.result(), 1);
}
